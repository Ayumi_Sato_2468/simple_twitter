<%@page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>簡易Twitter</title>
        <link href="./css/style.css" rel="stylesheet" type="text/css">
        <script type="text/javascript">
        <!--
        	function disp(){
        		window.alert('【削除してもよろしいですか？】');
        		}
        // -->
        </script>



    </head>
    <body>
    	<h1>簡易Twitter</h1>
        <div class="header">
			<c:if test="${ empty loginUser }">
				<a href="login">ログイン</a>
				<a href="signup">登録する</a>
			</c:if>
			<c:if test="${ not empty loginUser }">
				<a href="./">ホーム</a>
				<a href="setting">設定</a>
				<a href="logout">ログアウト</a>
			</c:if>
			<div class="dateFilter">
				<form action="./" method="get">
					日付 <input type="date" name="start"value="${start}"/>～
					<input type="date" name="end"value="${end}" />
					<input type="submit" value="絞込"/>
				</form>
			</div>
		</div>

		<c:if test="${ not empty loginUser }">
			<div class="profile">
				<div class="name"><h2><c:out value="${loginUser.name}" /></h2></div>
				<div class="account">@<c:out value="${loginUser.account}" /></div>
				<div class="description"><c:out value="${loginUser.description}" /></div>
			</div>
		</c:if>

		<c:if test="${ not empty errorMessages }">
			<div class="errorMessages">
				<ul>
					<c:forEach items="${errorMessages}" var="errorMessage">
						<li><c:out value="${errorMessage}" />
					</c:forEach>
				</ul>
			</div>
		<c:remove var="errorMessages" scope="session" />
		</c:if>


			<c:if test="${ isShowMessageForm }">
			<div class="form-area">
				<form action="message" method="post">
					いま、どうしてる？<br />
					<textarea name="text" cols="100" rows="5" class="tweet-box" ></textarea>
					<br />
					<input type="submit" value="つぶやく"/>（140文字まで）
				</form>
				</div>
			</c:if>


		<div class="messages">
			<c:forEach items="${messages}" var="message">
				<div class="message">
					<div class="account-name">
						<span class="name"><c:out value="${message.name}" /></span>
						<span class="account">
                			<a href="./?user_id=<c:out value="${message.userId}"/> ">
                				<c:out value="${message.account}" />
                			</a>
                		</span>
            		</div>
            		<div class="text"><pre><c:out value="${message.text}" /></pre></div>
					<div class="date"><fmt:formatDate value="${message.createdDate}" pattern="yyyy/MM/dd HH:mm:ss" /></div>
            		<c:if test="${ not empty loginUser and loginUser.id == message.userId }">
        				<a href="edit?id=<c:out value="${message.id}"/> "><input type="submit" value="編集"/></a>
        				<form style="display:inline" action="deleteMessage" method="post">
        					<input name="messageId" value="${message.id}" id="id" type="hidden"/><input type="submit" value="削除"onClick="disp()"/>
        				</form>
        			</c:if>
        		</div>

        		<div class="comments">
        			<c:forEach items="${comments}" var="comment">
						<c:if test="${ comment.messageId == message.id }">
							<div class="comment">
        						<div class="account-name">
        							<span class="name"><c:out value="${comment.name}" /></span>
									<span class="account">
                						<a href="./?user_id=<c:out value="${comment.userId}"/> ">
                							<c:out value="${comment.account}" />
                						</a>
                					</span>
            					</div>
            					<div class="text"><pre><c:out value="${comment.text}" /></pre></div>
								<div class="date"><fmt:formatDate value="${comment.createdDate}" pattern="yyyy/MM/dd HH:mm:ss" /></div>
							</div>
						</c:if>
					</c:forEach>
					<c:if test="${ isShowMessageForm }">
						<div class="comment-form-area">
							<form action="comment" method="post">
								<input name="messageId" value="${message.id}" id="id" type="hidden"/>
								<textarea name="text" cols="20" rows="3" class="comment-box"></textarea>
								<br />
								<input type="submit" value="返信"/>（140文字まで）
							</form>
						</div>
					</c:if>

        		</div>
    		</c:forEach>
		</div>
		<div class="copyright"> Copyright(c)Ayumi Sato</div>
    </body>
</html>