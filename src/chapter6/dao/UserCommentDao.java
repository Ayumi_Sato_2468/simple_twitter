package chapter6.dao;

import static chapter6.utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import chapter6.beans.UserComment;
import chapter6.exception.SQLRuntimeException;

public class UserCommentDao {

	public List<UserComment> select(Connection connection ,int num) {

		//SQL文を実行するために使用
		PreparedStatement ps = null;

		try {
			StringBuilder sql = new StringBuilder();

			//参照
			sql.append("SELECT ");

			//カラム名を別名へ
			sql.append("    comments.id as id, ");
			sql.append("    comments.text as text, ");
			sql.append("    comments.user_id as user_id, ");
			sql.append("    comments.message_id, ");
			sql.append("    users.account as account, ");
			sql.append("    users.name as name, ");
			sql.append("    comments.created_date as created_date, ");
			sql.append("    comments.updated_date as updated_date ");

			//comments テーブルから
			sql.append("FROM comments ");

			//userテーブルとcommentsテーブルを内部結合
			sql.append("INNER JOIN users ");

			//結合条件はmessage users
			sql.append("ON comments.user_id = users.id ");

			//昇順でソートする 最大num（1000）行分を指定
			sql.append("ORDER BY created_date ASC limit " + num);

			//Dbと接続し、sqlに記載された内容を実行準備
			ps = connection.prepareStatement(sql.toString());

			//SELECT文を実行し、rsに結果を格納
			ResultSet rs = ps.executeQuery();

			//messagesにSELECT文の実行結果を格納
			List<UserComment> comments = toUserComment(rs);

			//messegeを返す
			return comments;

		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}


	private List<UserComment> toUserComment(ResultSet rs) throws SQLException {

		List<UserComment> comments = new ArrayList<UserComment>();

		try {
			while (rs.next()) {
				UserComment comment = new UserComment();

				comment.setId(rs.getInt("id"));
				comment.setText(rs.getString("text"));
				comment.setUserId(rs.getInt("user_id"));
				comment.setAccount(rs.getString("account"));
				comment.setName(rs.getString("name"));
				comment.setCreatedDate(rs.getTimestamp("created_date"));
				comment.setUpdatedDate(rs.getTimestamp("updated_date"));
				comment.setMessageId(rs.getInt("message_id"));

				comments.add(comment);
			}
			return comments;

		} finally {
			close(rs);
		}
	}
}