package chapter6.dao;

import static chapter6.utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import chapter6.beans.Message;
import chapter6.exception.NoRowsUpdatedRuntimeException;
import chapter6.exception.SQLRuntimeException;

public class MessageDao {

	public void insert(Connection connection, Message message) {

		PreparedStatement ps = null;

		try {
			StringBuilder sql = new StringBuilder();

			sql.append("INSERT INTO messages ( ");
			sql.append("    user_id, ");
			sql.append("    text, ");
			sql.append("    created_date, ");
			sql.append("    updated_date ");
			sql.append(") VALUES ( ");
			sql.append("    ?, ");        // user_id
			sql.append("    ?, ");        // text
			sql.append("    CURRENT_TIMESTAMP, ");  // createdDate
			sql.append("    CURRENT_TIMESTAMP ");  // updatedDate
			sql.append(")");

			ps = connection.prepareStatement(sql.toString());

			ps.setInt(1, message.getUserId());
			ps.setString(2, message.getText());

			ps.executeUpdate();

		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}

	public void update(Connection connection, Message message) {

		PreparedStatement ps = null;

		try {
			StringBuilder sql = new StringBuilder();

			sql.append("UPDATE messages SET ");
			sql.append(" text = ? ");
			sql.append("WHERE id = ? ");

			ps = connection.prepareStatement(sql.toString());

			ps.setString(1, message.getText());
			ps.setInt(2, message.getId());

			//上記のSQL文を実行
			int count = ps.executeUpdate();

			if (count == 0) {
				throw new NoRowsUpdatedRuntimeException();
			}
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}

	public Message select(Connection connection, int messageId) {

		PreparedStatement ps = null;

		try {
			StringBuilder sql = new StringBuilder();

			sql.append("SELECT ");
			sql.append("  id, ");
			sql.append("  text, ");
			sql.append("  user_id, ");
			sql.append("  created_date ");
			sql.append("FROM ");
			sql.append(" messages ");
			sql.append("WHERE id = ? ");

			ps = connection.prepareStatement(sql.toString());

			//添え字はSQL文の？の数を表す
			ps.setInt(1, messageId);

			//SELECT文を実行し、rsに結果を格納
			ResultSet rs = ps.executeQuery();

			//SERECT文の結果をtoUserMessageメソッドへ渡し、返ってきた値をmessagesへ格納
			List<Message> messages = toUserMessage(rs);

			if(messages.isEmpty()) {
				return null;
			}else {
			//messegeを返す
			return messages.get(0);
			}

		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	 }

	private List<Message> toUserMessage(ResultSet rs) throws SQLException {

		List<Message> messages = new ArrayList<Message>();

		try {
			while (rs.next()) {
				Message message = new Message();

				message.setId(rs.getInt("id"));
				message.setText(rs.getString("text"));
				message.setUserId(rs.getInt("user_id"));
				message.setCreatedDate(rs.getTimestamp("created_date"));

				messages.add(message);
			}
			return messages;

		} finally {
			close(rs);
		}
	}

	public void delete(Connection connection, int messageId) {

		PreparedStatement ps = null;

		try {
			StringBuilder sql = new StringBuilder();

			sql.append("DELETE ");
			sql.append("FROM ");
			sql.append(" messages ");
			sql.append("WHERE id = ? ");

			ps = connection.prepareStatement(sql.toString());

			//添え字はSQL文の？の数を表す
			ps.setInt(1, messageId);

			//上記のSQL文を実行
			int count = ps.executeUpdate();

			if (count == 0) {
				throw new NoRowsUpdatedRuntimeException();
			}
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);

		}
	}
}

