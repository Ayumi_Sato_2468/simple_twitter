package chapter6.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;

import chapter6.beans.User;
import chapter6.service.UserService;

//signup（登録ボタン押下後）を表示する際に実行
@WebServlet(urlPatterns = { "/signup" })
public class SignUpServlet extends HttpServlet {
	
	@Override
	//登録画面表示のみ。getにてHTTPのリクエストを表し、レスポンスを表す（getの受け口を作成）
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws IOException, ServletException {
		
		//signupへ処理を移す
		request.getRequestDispatcher("signup.jsp").forward(request, response);
	}
	
	
	@Override
	//postにてHTTPのリクエストを表し、レスポンスを表す（postの受け口を作成）
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws IOException, ServletException {
		
		//ArrayList作成
		List<String> errorMessages = new ArrayList<String>();
		
		//getUserの情報をuserへ
		User user = getUser(request);
		
		//isValidを呼び出し、user情報とuser,ArrayListのerrorMessageを渡す
		if (!isValid(user, errorMessages)) {
			//リクエストスコープに格納
			request.setAttribute("errorMessages", errorMessages);
			
			//signup.jspへ処理を移す
			request.getRequestDispatcher("signup.jsp").forward(request, response);
			
			//処理を終わる
			return;
		}
		
		// service UserServiceのinsertへ処理を移す user情報渡す
		new UserService().insert(user);
		//リダイレクト
		response.sendRedirect("./");
	}
	
	private User getUser(HttpServletRequest request) throws IOException, ServletException {
		
		//Userクラスのuserを作成
		User user = new User();	
		
		//userへgetParameterにて取得した名前（name）をvalueにセット
		user.setName(request.getParameter("name"));
		
		//userへgetParameterにて取得したアカウント（account）をvalueにセット
		user.setAccount(request.getParameter("account"));
		
		//userへgetParameterにて取得したパスワード（password）をvalueにセット
		user.setPassword(request.getParameter("password"));
		
		//userへgetParameterにて取得したメールアドレス（email）をvalueにセット
		user.setEmail(request.getParameter("email"));
		
		//userへgetParameterにて取得した解説（description）をvalueにセット
		user.setDescription(request.getParameter("description"));
		
		//戻り値としてuserに返す（更新される）
		return user;
	}
	
	private boolean isValid(User user, List<String> errorMessages) {
		
		//nameにbeansのuserから名前を取得し代入
		String name = user.getName();
		
		//accountにbeansのuseからアカウントを取得代入
		String account = user.getAccount();
		
		//passwordにbeansのuserからパスワードを取得し代入
		String password = user.getPassword();
		
		//emailにbeansのuserからメールアドレスを取得し代入
		String email = user.getEmail();
		
		User duplicationUser = new UserService().select(account);
		
		//もし名前が空文字ではなく、20文字以上なら
		if (!StringUtils.isBlank(name) && (20 < name.length())) {
			errorMessages.add("名前は20文字以下で入力してください");
		}
		
		//もしアカウント名が空文字なら
		if (StringUtils.isBlank(account)) {
			errorMessages.add("アカウント名を入力してください");
		}
		
		//そうでなくとももしアカウント名は20文字以上なら
		else if (20 < account.length()) {
			errorMessages.add("アカウント名は20文字以下で入力してください");
		}
		
		//もしパスワードが空なら
		if (StringUtils.isBlank(password)) {
			errorMessages.add("パスワードを入力してください");
		}
		
		//もしアドレスが空文字かつ50文字以上なら
		if (!StringUtils.isBlank(email) && (50 < email.length())) {
			errorMessages.add("メールアドレスは50文字以下で入力してください");
			
		}
		if(duplicationUser != null) {
			errorMessages.add("アカウント名が重複しています");
		}
		
		//もしArrayList errorMassagesに格納された値の個数が0でないなら
		if (errorMessages.size() != 0) {
			return false;
		}
		return true;
	}
}